import React from 'react';
import './App.css';
import { SpreadsheetComponent, SheetsDirective, SheetDirective, ColumnsDirective, RangesDirective, RangeDirective, RowsDirective, RowDirective, CellsDirective, CellDirective, ColumnDirective } from '@syncfusion/ej2-react-spreadsheet';
import { SampleBase } from './sample-base';
import { clearData } from './clearData'
export default class App extends SampleBase {
  constructor() {
      super(...arguments);
      this.boldRight = { fontWeight: 'bold', textAlign: 'right' };
      this.bold = { fontWeight: 'bold' };
  }

  onCreated() {
      if (this.spreadsheet.sheets[this.spreadsheet.activeSheetIndex].name === 'Car Sales Report' && !this.spreadsheet.isOpen) {
          const data = localStorage.getItem("jsonData")
          if(data){
            this.spreadsheet.openFromJson({file:JSON.parse(data).jsonObject})
          }
          this.spreadsheet.cellFormat({ fontWeight: 'bold', textAlign: 'center', verticalAlign: 'middle', AllowEditOnDblClick: true }, 'A1:N1');
          this.spreadsheet.numberFormat('$#,##0.00', 'F2:F31');
      }
  }

  saveJSONData = () => {
    this.spreadsheet.saveAsJson().then(data=>{
      localStorage.setItem("jsonData", JSON.stringify(data))
    })
  }

  fullyClearJSONData = () => {
    const data = localStorage.getItem("jsonData")
    if(data){
      localStorage.removeItem("jsonData")
    }
    this.spreadsheet.openFromJson({file:clearData.jsonObject})
  }

  clearJSONData = () => {
    this.spreadsheet.openFromJson({file:clearData.jsonObject})
  }

  render() {
      return (
        <div>
          <div className="action-buttons">
            <button className="action-button save" onClick={()=>this.saveJSONData()}> Save </button>
            <button className="action-button cancel" onClick={()=>this.clearJSONData()}> Clear Data </button>
            <button className="action-button cancel" onClick={()=>this.fullyClearJSONData()}>Fully Clear Data </button>
          </div>
          <div className='control-pane'>
              <div className='control-section spreadsheet-control'>
                  <SpreadsheetComponent ref={(ssObj) => { this.spreadsheet = ssObj; }} created={this.onCreated.bind(this)}>
                      <SheetsDirective>
                          <SheetDirective name="Car Sales Report">
                              <RangesDirective>
                                  <RangeDirective ></RangeDirective>
                              </RangesDirective>
                              <RowsDirective>
                                  <RowDirective index={30}>
                                      <CellsDirective>
                                          <CellDirective index={4} value="Total Amount:" style={this.boldRight}></CellDirective>
                                          <CellDirective formula="=SUM(F2:F30)" style={this.bold}></CellDirective>
                                      </CellsDirective>
                                  </RowDirective>
                              </RowsDirective>
                              <ColumnsDirective>
                                  <ColumnDirective width={180}></ColumnDirective>
                                  <ColumnDirective width={130}></ColumnDirective>
                                  <ColumnDirective width={130}></ColumnDirective>
                                  <ColumnDirective width={180}></ColumnDirective>
                                  <ColumnDirective width={130}></ColumnDirective>
                                  <ColumnDirective width={120}></ColumnDirective>
                              </ColumnsDirective>
                          </SheetDirective>
                      </SheetsDirective>
                  </SpreadsheetComponent>
              </div>
          </div>
        </div>
      );
  }
}